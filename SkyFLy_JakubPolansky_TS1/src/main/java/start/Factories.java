package start;


import dbutils.SkyFlyEMF;

import entities.Airplane;
import entities.Airport;
import entities.Carrier;
import entities.Flight;
import entities.Employee;
import entities.Reservation;
import java.util.Date;
import javax.persistence.EntityManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author peter
 */
public class Factories {
  public Factories() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();

    em.getTransaction().begin();
    
    Carrier carrierSample1 = new Carrier("Lufthansa", "Germany", "1960");
    em.persist(carrierSample1);
//    em.getTransaction().commit();
   
//    em.getTransaction().begin();
    Carrier carrierSample2 = new Carrier("Air France", "France", "1970");
    em.persist(carrierSample2);

    Airplane airplaneSample1 = new Airplane(60, carrierSample1);
    airplaneSample1.setCarrier(carrierSample1);
    em.persist(airplaneSample1);
    
    Airplane airplaneSample2 = new Airplane(54, carrierSample1);
    airplaneSample2.setCarrier(carrierSample1);
    em.persist(airplaneSample2);
        
    Airplane airplaneSample3 = new Airplane(48, carrierSample2);
    airplaneSample3.setCarrier(carrierSample2);
    em.persist(airplaneSample3);

    Employee employeeSample1 = new Employee("Marek", "Vanecek", "4545808010", "password", carrierSample2);
    em.persist(employeeSample1);

    Employee employeeSample2 = new Employee("Vojtech", "Orisek", "9876543210", "password", carrierSample1);
    em.persist(employeeSample2);
    
    Airport airportSample1 = new Airport("Warsaw Chopin", "WAW");
    em.persist(airportSample1);
    
    Airport airportSample2 = new Airport("Budapest International", "BUD");
    em.persist(airportSample2);
    
    Airport airportSample3 = new Airport("Vienna International", "VIE");
    em.persist(airportSample3);
    
    Airport airportSample4 = new Airport("Barcelona International", "BAR");
    em.persist(airportSample4);
    
    Airport airportSample5 = new Airport("Boston International", "BOT");
    em.persist(airportSample5);
    
    Airport airportSample6 = new Airport("Miami International", "MIA");
    em.persist(airportSample6);
    
    Flight flightSample1 = new Flight(airportSample1.getShortcut(), airportSample2.getShortcut(), 120, new Date(117, 1, 1, 8, 0), 300, airplaneSample1, null);
    em.persist(flightSample1);
    
    Flight flightSample2 = new Flight(airportSample3.getShortcut(), airportSample4.getShortcut(), 300, new Date(118, 2, 3, 5, 0), 500, airplaneSample2, null);
    em.persist(flightSample2);
    
    Flight flightSample3 = new Flight(airportSample5.getShortcut(), airportSample6.getShortcut(), 89, new Date(117, 5, 6, 7, 55), 900, airplaneSample1, null);
    em.persist(flightSample3);
        
    Flight flightSample4 = new Flight(airportSample1.getShortcut(), airportSample5.getShortcut(), 114, new Date(117, 2, 8, 5, 0), 700, airplaneSample2, null);
    em.persist(flightSample4);

    Flight flightSample5 = new Flight(airportSample1.getShortcut(), airportSample2.getShortcut(), 89, new Date(117, 5, 6, 2, 55), 900, airplaneSample1, null);
    em.persist(flightSample5);

    Flight flightSample6 = new Flight(airportSample1.getShortcut(), airportSample2.getShortcut(), 89, new Date(117, 5, 6, 2, 55), 900, airplaneSample1, null);
    em.persist(flightSample3);
    
    Reservation reservationSample1 = new Reservation("2222222222", 15, flightSample1);
    em.persist(reservationSample1);

    Reservation reservationSample2 = new Reservation("3333333333", 5, flightSample1);
    em.persist(reservationSample2);

    Reservation reservationSample3 = new Reservation("4848484848", 33, flightSample1);
    em.persist(reservationSample3);

    em.getTransaction().commit();
  }
}
