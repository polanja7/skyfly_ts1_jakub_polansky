package start;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author peter
 */

import dbutils.View;

import views.MainFrame;
import views.FlightSearch;
import java.awt.BorderLayout;

public class Main {
  private static MainFrame frame;
  
  public static void main(String[] args) {
    // new Factories();
    new View();
    
    frame = new MainFrame();

    frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
    
    frame.getContentPane().setLayout(new BorderLayout());
    frame.getContentPane().add(new FlightSearch());
    
    frame.pack();
    frame.setVisible(true);
  }

  public static MainFrame getFrame() {
    return frame;
  }
}