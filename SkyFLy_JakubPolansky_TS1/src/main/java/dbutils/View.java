/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbutils;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author peter
 */
public class View {
  public View() {
    departingFromPrague();
  }
  
  private void departingFromPrague() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    Query q = em.createNativeQuery("CREATE VIEW flights_departing_from_prague AS SELECT * FROM flight WHERE departure_airport=PRG");
  }
}
