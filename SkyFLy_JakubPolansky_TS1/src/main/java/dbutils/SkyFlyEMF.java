/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbutils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author peter
 */
public class SkyFlyEMF {
  private static String unitName = "CVUT_DBS_PU";
  private static EntityManagerFactory emf;

  public static EntityManagerFactory getEmf() {
    if(emf == null) {
      emf = Persistence.createEntityManagerFactory(unitName);
    }
    return emf;
  }

  public static void reloadEmf() {
    SkyFlyEMF.emf = Persistence.createEntityManagerFactory(unitName);
  }
  
  public static void setUnitName(String unitName) {
    SkyFlyEMF.unitName = unitName;
  }
}
