/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dbutils.SkyFlyEMF;

import java.util.List;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author peter
 */
@Entity
@Table
public class Flight implements Serializable {
  @PersistenceUnit(unitName = "CVUT_DBS_PU")
  private static EntityManagerFactory emf;
  
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "departure_airport", nullable = false)
  private String departureAirport;
  
  @Column(name = "arrival_airport", nullable = false)
  private String arrivalAirport;
  
  @Column(name = "duration", nullable = false)
  private int duration;
  
  @Column(name = "time_of_departure", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date timeOfDeparture;
  
  @Column(name = "price", nullable = false)
  private int price;
  
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="airplane_id")
  private Airplane airplane;
  
  @OneToMany(mappedBy="flight", orphanRemoval=true, cascade = CascadeType.REMOVE)
  private List<Reservation> reservations;

  public Flight() {};
  
  /**
  * Flight constructor
  * 
  * @param departureAirport departing airport
  * @param arrivalAirport arriving airport
  * @param duration flight duration
  * @param timeOfDeparture time of departing
  * @param price ticket price
  * @param airplane which airplane will flight
  * @param reservations list of all reservations for that flight
  */
  public Flight(String departureAirport, String arrivalAirport, int duration, Date timeOfDeparture, int price, Airplane airplane, List<Reservation> reservations) {
    this.departureAirport = departureAirport;
    this.arrivalAirport = arrivalAirport;
    this.duration = duration;
    this.timeOfDeparture = timeOfDeparture;
    this.price = price;
    this.airplane = airplane;
    this.reservations = reservations;
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Airplane getAirplane() {
    return airplane;
  }

  public String getDepartureAirport() {
    return departureAirport;
  }

  public String getArrivalAirport() {
    return arrivalAirport;
  }

  public int getDuration() {
    return duration;
  }

  public Date getTimeOfDeparture() {
    return timeOfDeparture;
  }

  public int getPrice() {
    return price;
  }

  public List<Reservation> getReservations() {
    return reservations;
  }

  public void setDepartureAirport(String departureAirport) {
    this.departureAirport = departureAirport;
  }

  public void setArrivalAirport(String arrivalAirport) {
    this.arrivalAirport = arrivalAirport;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public void setTimeOfDeparture(Date timeOfDeparture) {
    this.timeOfDeparture = timeOfDeparture;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public void setAirplane(Airplane airplane) {
    this.airplane = airplane;
  }
  
  public Integer getEmptySeats() {
    return getAirplane().getCapacity() - getReservations().size();
  }
  
  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Flight)) {
      return false;
    }
    Flight other = (Flight) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Flight[ id=" + id + " ]";
  }
  
  
  /**
  * This method gets all the flights from table Flight
  * and returns its list.
  * 
  * @return Flight list of the Flights
  */
  public static List<Flight> getAll() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    return em.createQuery("SELECT f FROM Flight f").getResultList();
  }
  
  
  /**
  * This method gets and returns list of flight's departureAirport 
  * and arrivalAirport from table airport
  * 
  * @return Airport list of the Airports
  */
  public List<Airport> getAirports() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    
    return em.createQuery("SELECT a FROM Airport a WHERE a.departureAirport = :departure_airport OR a.arrivalAirport = :arrival_airport")
      .setParameter("departure_airport", this.getDepartureAirport())
      .setParameter("arrival_airport", this.getArrivalAirport())
      .getResultList();
  }
  
  
  /**
  * This method gets unique departureAirports
  * and returns its list
  * 
  * @return String list of the Strings
  */
  public static List<String> getAllDepartureAirport() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    return em.createQuery("SELECT DISTINCT f.departureAirport FROM Flight f").getResultList();
  }
  
  
  /**
  * This method gets unique arrivalAirports
  * and returns its list.
  * 
  * @return String list of the Strings
  */
  public static List<String> getAllArrivalAirport() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    return em.createQuery("SELECT DISTINCT f.arrivalAirport FROM Flight f").getResultList();
  }
  
  /**
  * This method takes 3 arguments.
  * 
  * @param departureAirport Departing airport
  * @param arrivalAirport Arriving airport
  * @param timeOfDeparture Time of plane departure
  * @return Flight list of the flights
  */
  public static List<Flight> getAllFlightsFromQuery(String departureAirport, String arrivalAirport, Date timeOfDeparture) {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    
    return em.createQuery("SELECT f FROM Flight f WHERE f.departureAirport = :departureAirport AND f.arrivalAirport = :arrivalAirport AND f.timeOfDeparture >= :timeOfDeparture")
        .setParameter("departureAirport", departureAirport)
        .setParameter("arrivalAirport", arrivalAirport) 
        .setParameter("timeOfDeparture", timeOfDeparture)
        .getResultList();
  }
  
  /**
  * This method takes String of the date
  * and parses it to the date data type
  * 
  * @param dateToParse string of the date
  * @return Date dateFrom String
  * @throws ParseException if parsing fails, throws exception
  */
  public static Date getDateFromString(String dateToParse) throws ParseException {    
    return new SimpleDateFormat("yyyyMMddHHmm").parse(dateToParse);
  }
}
