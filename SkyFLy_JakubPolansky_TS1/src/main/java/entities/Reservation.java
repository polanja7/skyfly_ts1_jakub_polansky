/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dbutils.SkyFlyEMF;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

import javax.validation.constraints.Size;
import javax.validation.constraints.DecimalMin;

/**
 *
 * @author peter
 */
@Entity
@Table
public class Reservation implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  @Size(max = 10, min = 10)
  @Column(name = "passenger_birth_number", nullable = false)
  String passengerBirthNumber;

  @DecimalMin(value = "1")
  @Column(name = "seat_number", nullable = false)
  Integer seatNumber;
  
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="flight_id")
  private Flight flight;

  public Reservation() {}
  
  /**
  * Reservation constructor
  * 
  * @param passengerBirthNumber passenger identificator
  * @param seatNumber passenger seatNumber
  * @param flight selected flight by passenger
  */
  public Reservation(String passengerBirthNumber, Integer seatNumber, Flight flight) {
    this.passengerBirthNumber = passengerBirthNumber;
    this.seatNumber = seatNumber;
    this.flight = flight;
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPassengerBirthNumber() {
    return passengerBirthNumber;
  }

  public Integer getSeatNumber() {
    return seatNumber;
  }

  public Flight getFlight() {
    return flight;
  }

  public void setPassengerBirthNumber(String passengerBirthNumber) {
    this.passengerBirthNumber = passengerBirthNumber;
  }

  public void setSeatNumber(Integer seatNumber) {
    this.seatNumber = seatNumber;
  }

  public void setFlight(Flight flight) {
    this.flight = flight;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Reservation)) {
      return false;
    }
    Reservation other = (Reservation) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Reservation[ id=" + id + " ]";
  }
  
  /**
  * This method gets passenger birth number and
  * looks to database for its all reservations.
  * 
  * @param birthNumber passenger birth number
  * @return Reservation list of Reservations
  */
  public static List<Reservation> getAllReservationsFromQuery(String birthNumber) {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    
    return em.createQuery("SELECT r FROM Reservation r WHERE r.passengerBirthNumber = :birthNumber")
        .setParameter("birthNumber", birthNumber)
        .getResultList();
  }
  
  /**
  * This method gets reservation as an parameter 
  * and removes it from the database.
  * 
  * @param reservation Reservation object.
  */
  public static void removeReservation(Reservation reservation) {  
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
 
    if (!em.contains(reservation)) {
      reservation = em.merge(reservation);
    }
    
    em.getTransaction().begin();
    em.remove(reservation);
    em.getTransaction().commit();
  }
}
