/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dbutils.SkyFlyEMF;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author peter
 */
@Entity
@Table
public class Airplane implements Serializable {
  
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(nullable = false)
  private int capacity;
  
  @OneToMany(mappedBy="airplane")
  private List<Flight> flights;
  
  public List<Flight> getFlights() {
    return flights;
  }
  
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name = "carrier_id", referencedColumnName="id")
  private Carrier carrier;

  public Carrier getCarrier() {
    return carrier;
  }

  public Airplane() {}
  
  /**
  * Airplane constructor
  * 
  * @param capacity passenger capacity of the airplane
  * @param carrier airplanes carrier
  */
  public Airplane(int capacity, Carrier carrier) {
    this.capacity = capacity;
    this.carrier = carrier;
  }
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }

  public int getCapacity() {
    return capacity;
  }

  public void setCarrier(Carrier carrier) {
    this.carrier = carrier;
  }
  
  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Airplane)) {
      return false;
    }
    Airplane other = (Airplane) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Airplane[ id=" + id + " ]";
  }
  
  /**
  * This method gets all the flights from table Airplane 
  * and then returns them as a list.
  * 
  * @return Airplane returns list of airplanes
  */
  public static List<Airplane> getAll() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    return em.createQuery("SELECT a FROM Airplane a").getResultList();
  }
}
