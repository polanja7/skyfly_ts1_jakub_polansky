/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author david
 */

@Entity
@Table
public class Passenger implements Serializable {
  @PersistenceUnit(unitName = "CVUT_DBS_PU")
  private static EntityManagerFactory emf;
  
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "birhtNumber", nullable = false)
  private String birthNumber;
  
  @Column(name = "firstName", nullable = false)
  private String firstName;
 
  @Column(name = "surname", nullable = false)
  private String surname;

  public Passenger() {}
  
  /**
  * Passenger constructor
  * 
  * @param birthNumber passenger identificator
  * @param firstName passenger first name
  * @param surname passenger last name
  */
  public Passenger(String birthNumber, String firstName, String surname) {
    this.birthNumber = birthNumber;
    this.firstName = firstName;
    this.surname = surname;
  }
  
  public String getBirhtNumber() {
      return birthNumber;
  }

  public String getFirstName() {
      return firstName;
  }

  public String getSurname() {
      return surname;
  }  

  @Override
  public String toString() {
    return "entities.Passenger[ id=" + birthNumber + " ]";
  }
}
