/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dbutils.SkyFlyEMF;

import java.util.List;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author peter
 */
@Entity
@Table
public class Employee implements Serializable {
  private static Employee currentEmployee;
  
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "first_name", nullable = false)
  String firstName;
  
  @Column(name = "surname", nullable = false)
  String surname;

  @Column(name = "birth_number", nullable = false, unique = true)
  String birthNumber;

  @Column(name = "password", nullable = false)
  String password;
  
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="carrier_id")
  private Carrier carrier;

  public Employee() {};
  
  /**
  * Employee constructor
  * 
  * @param firstName emplyees first name
  * @param surname employeees last name
  * @param birthNumber employees birth number
  * @param password employees password
  * @param carrier employees employer
  */
  public Employee(String firstName, String surname, String birthNumber, String password, Carrier carrier) {
    this.firstName = firstName;
    this.surname = surname;
    this.birthNumber = birthNumber;
    this.password = password;
    this.carrier = carrier;
  }

  
  /**
  * This method uses its parameters to check,
  * if user is in the database. If not,
  * return false, otherwise, returns true.
  * It uses one another method - findBirthNumber()
  * 
  * @param birthNumber id of employee
  * @param password password of employee
  * @return boolean
  */
  public static boolean authenticate(String birthNumber, String password) {
    Employee employee = findByBirthNumber(birthNumber);
    
    if (employee != null) {
      if (employee.getPassword().equals(password)) {
        currentEmployee = employee;
        return true;
      }
    }
    
    return false;
  }
  
  
  /**
  * This method is searching for the employee by 
  * its birthNumber. If employee is found, then
  * returns employee, otherwise returns null.
  * 
  * @param birthNumber id of employee
  * @return Employee or null
  */
  public static Employee findByBirthNumber(String birthNumber) {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    Query employeeQuery = em.createQuery("SELECT e FROM Employee e WHERE e.birthNumber = :birthNumber");
    employeeQuery.setParameter("birthNumber", birthNumber);
    
    List<Employee> employeeResult = employeeQuery.getResultList();
    
    if (employeeResult.size() > 0) {
      return employeeResult.get(0);
    }
    
    return null;
  }
  
  public String getFirstName() {
    return firstName;
  }

  public String getSurname() {
    return surname;
  }

  public String getBirthNumber() {
    return birthNumber;
  }

  public String getPassword() {
    return password;
  }

  public Carrier getCarrier() {
    return carrier;
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public static Employee getCurrentEmployee() {
    return currentEmployee;
  }

  public static void setCurrentEmployee(Employee currentEmployee) {
    Employee.currentEmployee = currentEmployee;
  }
  
  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Employee)) {
      return false;
    }
    Employee other = (Employee) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Employee[ id=" + id + " ]";
  }
  
}
