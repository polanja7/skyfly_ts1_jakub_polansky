/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dbutils.SkyFlyEMF;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author peter
 */
@Entity
@Table
public class Carrier implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", nullable = false)
  String name;
  
  @Column(name = "country", nullable = false)
  String country;
  
  @Column(name = "year_of_foundation", nullable = false)
  String yearOfFoundation;
  
  @OneToMany(mappedBy="carrier", cascade=CascadeType.PERSIST)
  private List<Airplane> airplanes;
  
  @OneToMany(mappedBy="carrier")
  private List<Airplane> employees;
  
  public Carrier() {}
  
  /**
  * Carrier constructor
  * 
  * @param name carriers name
  * @param country carriers country
  * @param yearOfFoundation carriers year of foundation
  */
  public Carrier(String name, String country, String yearOfFoundation) {
    this.name = name;
    this.country = country;
    this.yearOfFoundation = yearOfFoundation;
  }
  
  public List<Airplane> getAirplanes() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    
    return em.createQuery("SELECT a FROM Airplane a WHERE a.carrier.id = :carrierId")
      .setParameter("carrierId", getId())
      .getResultList();
  }
  
  public List<Flight> getFlights() {
    List<Flight> flights = new ArrayList<Flight>();
    
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    
    for(Airplane airplane : this.getAirplanes()) {
      flights.addAll(airplane.getFlights());
      flights.addAll(em.createQuery("SELECT f FROM Flight f WHERE f.airplane.id = :airplaneId").setParameter("airplaneId", airplane.getId()).getResultList());
    }
    
    return flights;
  }

  public String getName() {
    return name;
  }

  public String getCountry() {
    return country;
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Carrier)) {
      return false;
    }
    Carrier other = (Carrier) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Carrier[ id=" + id + " ]";
  }
  
  
  /**
  * This method gets all the Carriers from table Carrier
  * and returns them.
  * 
  * @return Carrier returns list of Carriers
  */
  public static List<Carrier> getAll() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    return em.createQuery("SELECT c FROM Carrier c").getResultList();
  }
}
