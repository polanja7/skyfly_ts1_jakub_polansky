/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dbutils.SkyFlyEMF;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author peter
 */
@Entity
@Table
public class Airport implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  @Column(name = "name")
  String name;

  @Column(name = "shortcut", nullable = false, unique = true)
  String shortcut;

  public Airport() {}
  
  /**
  * Airport constructor
  * 
  * @param name Airport name
  * @param shortcut Airport name shortcut
  */
  public Airport(String name, String shortcut) {
    this.name = name;
    this.shortcut = shortcut;
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public String getShortcut() {
    return shortcut;
  }
  
  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Airport)) {
      return false;
    }
    Airport other = (Airport) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Airport[ id=" + id + " ]";
  }
 
  
  /**
  * This method gets all the airports from table Airport
  * and returns them as a list.
  * 
  * @return Airport returns list of Airports
  */
  public static List<Airport> getAll() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    return em.createQuery("SELECT a FROM Airport a").getResultList();
  }
  
  /**
  * This method gets all the flights where this airport
  * is listed as arrival airport, or departure airport
  * 
  * @return FLight returns list of Flights
  */
  public List<Flight> getFlights() {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    
    return em.createQuery("SELECT f FROM Flight f WHERE f.departureAirport = :departure_airport OR f.arrivalAirport = :arrival_airport")
      .setParameter("departure_airport", this.getShortcut())
      .setParameter("arrival_airport", this.getShortcut())
      .getResultList();
  }
  
  /**
  * This method takes Airport shortcut and 
  * looks for it in the database. If found,
  * returns list. Then from list is selected
  * first airport (airports has different shortcuts)
  * 
  * @param shortcut airport shortcut
  * @return Airport just Airport
  */
  public static Airport findByShortcut(String shortcut) {
    
    EntityManager em = SkyFlyEMF.getEmf().createEntityManager();
    List<Airport> airports = em.createQuery("SELECT a FROM Airport a WHERE a.shortcut = :shortcut")
      .setParameter("shortcut", shortcut)
      .getResultList();
      
    return airports.get(0);
  }
}
