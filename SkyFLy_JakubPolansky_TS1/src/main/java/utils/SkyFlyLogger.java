/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.logging.*;

/**
 *
 * @author peter
 */
public abstract class SkyFlyLogger {
    private static final Logger LOG = Logger.getLogger(SkyFlyLogger.class.getName());
    
    public static void notFound(String item) {
        LOG.log(Level.WARNING, "{0} not found!", item);
    }
    
    public static void hasBeenCreated(String item) {
        LOG.log(Level.INFO, "{0} has been created!", item);
    }
    
    public static void hasBeenDeleted(String item) {
        LOG.log(Level.INFO, "{0} has been deleted!", item);
    }
    
    public static void hasBeenUpdated(String item) {
        LOG.log(Level.INFO, "{0} has been updated!", item);
    }
    
    public static void numberOfResults(int numberOfResults) {
        if(numberOfResults == 1) {
            LOG.log(Level.INFO, numberOfResults + " result found!");
        }
        else {
            LOG.log(Level.INFO, numberOfResults + " results found!");
        }
    }
    
    public static void loggedIn(String userId) {
        LOG.log(Level.INFO, "{0} has been logged in!", userId);
    }
}
