package UnitTests;

import RefactoredClassesForTesting.TestableCarrier;
import entities.Carrier;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class CarrierUnitTest {
    private static String carrierName;
    private static String countryOfOrigin;
    private static String yearOfFoundation;

    @BeforeAll
    public static void setUp () {
        carrierName = "British Airways";
        countryOfOrigin = "United Kingdom";
        yearOfFoundation = "1965";
    }
    @Test
    public void CarrierConstructor_OneInstanceCreated_Returns1 () {
        // Arrange
        long carrierId = 20;
        try (MockedConstruction mocked = Mockito.mockConstruction(TestableCarrier.class)) {

            // Act
            new TestableCarrier(carrierId, carrierName, countryOfOrigin, yearOfFoundation);

            // Assert
            assertEquals(1, mocked.constructed().size());
        }
    }

    @Test
    public void equals_InputIsDifferentClass_ReturnsFalse () {
        // Arrange
        long carrierId = 20;
        String input = "Not a Carrier";
        Carrier theCarrier = new TestableCarrier(carrierId, carrierName, countryOfOrigin, yearOfFoundation);

        // Act
        boolean result = theCarrier.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsNull_ReturnsFalse () {
        // Arrange
        long carrierId = 20;
        Carrier input = null;
        Carrier theCarrier = new TestableCarrier(carrierId, carrierName, countryOfOrigin, yearOfFoundation);

        // Act
        boolean result = theCarrier.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsCarrierButDifferent_ReturnsFalse () {
        // Arrange
        long firstCarrierId = 20;
        long secondCarrierId = 50;
        Carrier input = new TestableCarrier(firstCarrierId, carrierName, countryOfOrigin, yearOfFoundation);
        Carrier theCarrier = new TestableCarrier(secondCarrierId, carrierName, countryOfOrigin, yearOfFoundation);

        // Act
        boolean result = theCarrier.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsTheSameCarrier_ReturnsTrue () {
        // Arrange
        long carrierId = 50;
        Carrier carrier = new TestableCarrier(carrierId, carrierName, countryOfOrigin, yearOfFoundation);

        // Act
        boolean result = carrier.equals(carrier);

        // Assert
        assertTrue(result);
    }
}
