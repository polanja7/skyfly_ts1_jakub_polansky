package UnitTests;

import RefactoredClassesForTesting.TestableAirplane;
import entities.Airplane;
import entities.Carrier;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.mock;

import org.mockito.MockedConstruction;
import org.mockito.Mockito;

public class AirplaneUnitTest {
    private static int capacity;
    private static Carrier mockedCarrier;

    @BeforeAll
    public static void setUp () {
        capacity = 150;
        mockedCarrier = mock(Carrier.class);
    }
    @Test
    public void AirplaneConstructor_OneInstanceCreated_Returns1 () {
        // Arrange
        long airplaneId = 20;
        try (MockedConstruction mocked = Mockito.mockConstruction(TestableAirplane.class)) {

            // Act
            new TestableAirplane(airplaneId, capacity, mockedCarrier);

            // Assert
            assertEquals(1, mocked.constructed().size());
        }
    }

    @Test
    public void equals_InputIsDifferentClass_ReturnsFalse () {
        // Arrange
        long airplaneId = 20;
        String input = "Not an Airplane";
        Airplane theAirplane = new TestableAirplane(airplaneId, capacity, mockedCarrier);

        // Act
        boolean result = theAirplane.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsNull_ReturnsFalse () {
        // Arrange
        long airplaneId = 20;
        Airplane input = null;
        Airplane theAirplane = new TestableAirplane(airplaneId,capacity, mockedCarrier);

        // Act
        boolean result = theAirplane.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsAirplaneButDifferent_ReturnsFalse () {
        // Arrange
        long firstAirplaneId = 20;
        long secondAirplaneId = 50;
        Airplane input = new TestableAirplane(firstAirplaneId, capacity, mockedCarrier);
        Airplane airplane = new TestableAirplane(secondAirplaneId, capacity, mockedCarrier);

        // Act
        boolean result = airplane.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsTheSameAirplane_ReturnsTrue () {
        // Arrange
        long airplaneId = 50;
        Airplane airplane = new TestableAirplane(airplaneId, capacity, mockedCarrier);

        // Act
        boolean result = airplane.equals(airplane);

        // Assert
        assertTrue(result);
    }
}
