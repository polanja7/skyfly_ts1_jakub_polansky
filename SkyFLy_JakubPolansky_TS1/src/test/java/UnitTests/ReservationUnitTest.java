package UnitTests;

import RefactoredClassesForTesting.TestableReservation;
import entities.Flight;
import entities.Reservation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class ReservationUnitTest {
    private static String passengerBirthNumber;
    private static int seatNumber;
    private static Flight mockedFlight;

    @BeforeAll
    public static void setUp () {
        passengerBirthNumber = "1234567890";
        seatNumber = 12;
        mockedFlight = mock(Flight.class);
    }


    @Test
    public void ReservationConstructor_OneInstanceCreated_Returns1 () {
        // Arrange
        long ReservationId = 20;
        try (MockedConstruction mocked = Mockito.mockConstruction(TestableReservation.class)) {

            // Act
            new TestableReservation(ReservationId, passengerBirthNumber, seatNumber, mockedFlight);

            // Assert
            assertEquals(1, mocked.constructed().size());
        }
    }

    @Test
    public void equals_InputIsDifferentClass_ReturnsFalse () {
        // Arrange
        long ReservationId = 20;
        String input = "Not a Reservation";
        Reservation theReservation = new TestableReservation(ReservationId, passengerBirthNumber, seatNumber, mockedFlight);

        // Act
        boolean result = theReservation.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsNull_ReturnsFalse () {
        // Arrange
        long ReservationId = 20;
        Reservation input = null;
        Reservation theReservation = new TestableReservation(ReservationId, passengerBirthNumber, seatNumber, mockedFlight);

        // Act
        boolean result = theReservation.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsReservationButDifferent_ReturnsFalse () {
        // Arrange
        long firstReservationId = 20;
        long secondReservationId = 50;
        Reservation input = new TestableReservation(firstReservationId, passengerBirthNumber, seatNumber, mockedFlight);
        Reservation theReservation = new TestableReservation(secondReservationId, passengerBirthNumber, seatNumber, mockedFlight);

        // Act
        boolean result = theReservation.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsTheSameReservation_ReturnsTrue () {
        // Arrange
        long ReservationId = 50;
        Reservation theReservation = new TestableReservation(ReservationId, passengerBirthNumber, seatNumber, mockedFlight);

        // Act
        boolean result = theReservation.equals(theReservation);

        // Assert
        assertTrue(result);
    }
}
