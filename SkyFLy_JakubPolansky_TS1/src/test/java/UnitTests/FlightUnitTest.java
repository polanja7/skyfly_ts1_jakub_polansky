package UnitTests;

import RefactoredClassesForTesting.TestableFlight;
import entities.Airplane;
import entities.Flight;
import entities.Reservation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class FlightUnitTest {
    private static String departureAirport;
    private static String arrivalAirport;
    private static int duration;
    private static Date timeOfDeparture;
    public static int price;
    public static Airplane mockedAirplane;
    public static List<Reservation> reservations;



    @BeforeAll
    public static void setUp () {
        departureAirport = "PRG";
        arrivalAirport = "LHR";
        duration = 120;
        timeOfDeparture = new Date(117, 1, 1, 8, 0);
        price = 150;
        mockedAirplane = mock(Airplane.class);
        reservations = new ArrayList<>();
    }


    @Test
    public void FlightConstructor_OneInstanceCreated_Returns1 () {
        // Arrange
        long FlightId = 20;
        try (MockedConstruction mocked = Mockito.mockConstruction(TestableFlight.class)) {

            // Act
            new TestableFlight(FlightId, departureAirport, arrivalAirport, duration, timeOfDeparture, price, mockedAirplane, reservations);

            // Assert
            assertEquals(1, mocked.constructed().size());
        }
    }

    @Test
    public void equals_InputIsDifferentClass_ReturnsFalse () {
        // Arrange
        long FlightId = 20;
        String input = "Not a Flight";
        Flight theFlight = new TestableFlight(FlightId, departureAirport, arrivalAirport, duration, timeOfDeparture, price, mockedAirplane, reservations);

        // Act
        boolean result = theFlight.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsNull_ReturnsFalse () {
        // Arrange
        long flightId = 20;
        Flight input = null;
        Flight theFlight = new TestableFlight(flightId, departureAirport, arrivalAirport, duration, timeOfDeparture, price, mockedAirplane, reservations);

        // Act
        boolean result = theFlight.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsFlightButDifferent_ReturnsFalse () {
        // Arrange
        long firstFlightId = 20;
        long secondFlightId = 50;
        Flight input = new TestableFlight(firstFlightId, departureAirport, arrivalAirport, duration, timeOfDeparture, price, mockedAirplane, reservations);
        Flight theFlight = new TestableFlight(secondFlightId, departureAirport, arrivalAirport, duration, timeOfDeparture, price, mockedAirplane, reservations);

        // Act
        boolean result = theFlight.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsTheSameFlight_ReturnsTrue () {
        // Arrange
        long FlightId = 50;
        Flight theFlight = new TestableFlight(FlightId, departureAirport, arrivalAirport, duration, timeOfDeparture, price, mockedAirplane, reservations);

        // Act
        boolean result = theFlight.equals(theFlight);

        // Assert
        assertTrue(result);
    }
}
