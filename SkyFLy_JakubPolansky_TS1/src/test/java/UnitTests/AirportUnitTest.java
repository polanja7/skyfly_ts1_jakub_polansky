package UnitTests;

import RefactoredClassesForTesting.TestableAirport;
import entities.Airport;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


import org.mockito.MockedConstruction;
import org.mockito.Mockito;

public class AirportUnitTest {
    private static String airportName;
    private static String airportShortcut;

    @BeforeAll
    public static void setUp () {
        airportName = "Vaclav Havel International Airport";
        airportShortcut = "PRG";
    }
    @Test
    public void AirportConstructor_OneInstanceCreated_Returns1 () {
        // Arrange
        long airportId = 20;
        try (MockedConstruction mocked = Mockito.mockConstruction(TestableAirport.class)) {

            // Act
            new TestableAirport(airportId, airportName, airportShortcut);

            // Assert
            assertEquals(1, mocked.constructed().size());
        }
    }

    @Test
    public void equals_InputIsDifferentClass_ReturnsFalse () {
        // Arrange
        long airportId = 20;
        String input = "Not an Airport";
        Airport theAirport = new TestableAirport(airportId, airportName, airportShortcut);

        // Act
        boolean result = theAirport.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsNull_ReturnsFalse () {
        // Arrange
        long airportId = 20;
        Airport input = null;
        Airport theAirport = new TestableAirport(airportId, airportName, airportShortcut);

        // Act
        boolean result = theAirport.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsAirportButDifferent_ReturnsFalse () {
        // Arrange
        long firstAirportId = 20;
        long secondAirportId = 50;
        Airport input = new TestableAirport(firstAirportId, airportName, airportShortcut);
        Airport airport = new TestableAirport(secondAirportId, airportName, airportShortcut);

        // Act
        boolean result = airport.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsTheSameAirport_ReturnsTrue () {
        // Arrange
        long airportId = 50;
        Airport airport = new TestableAirport(airportId, airportName, airportShortcut);

        // Act
        boolean result = airport.equals(airport);

        // Assert
        assertTrue(result);
    }
}
