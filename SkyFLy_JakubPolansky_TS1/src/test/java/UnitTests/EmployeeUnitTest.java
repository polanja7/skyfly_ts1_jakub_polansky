package UnitTests;

import RefactoredClassesForTesting.TestableEmployee;
import RefactoredClassesForTesting.TestableFlight;
import entities.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class EmployeeUnitTest {
    private static String firstName;
    private static String surname;
    private static String birthNumber;
    private static String password;
    public static Carrier mockedCarrier;

    @BeforeAll
    public static void setUp () {
        firstName = "James";
        surname = "Brown";
        birthNumber = "1111111111";
        password = "heslo";
        mockedCarrier = mock(Carrier.class);
    }


    @Test
    public void EmployeeConstructor_OneInstanceCreated_Returns1 () {
        // Arrange
        long EmployeeId = 20;
        try (MockedConstruction mocked = Mockito.mockConstruction(TestableEmployee.class)) {

            // Act
            new TestableEmployee(EmployeeId, firstName, surname, birthNumber, password, mockedCarrier);

            // Assert
            assertEquals(1, mocked.constructed().size());
        }
    }

    @Test
    public void equals_InputIsDifferentClass_ReturnsFalse () {
        // Arrange
        long EmployeeId = 20;
        String input = "Not an Employee";
        Employee theEmployee = new TestableEmployee(EmployeeId, firstName, surname, birthNumber, password, mockedCarrier);

        // Act
        boolean result = theEmployee.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsNull_ReturnsFalse () {
        // Arrange
        long EmployeeId = 20;
        Employee input = null;
        Employee theEmployee = new TestableEmployee(EmployeeId, firstName, surname, birthNumber, password, mockedCarrier);

        // Act
        boolean result = theEmployee.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsEmployeeButDifferent_ReturnsFalse () {
        // Arrange
        long firstEmployeeId = 20;
        long secondEmployeeId = 50;
        Employee input = new TestableEmployee(firstEmployeeId, firstName, surname, birthNumber, password, mockedCarrier);
        Employee theEmployee = new TestableEmployee(secondEmployeeId, firstName, surname, birthNumber, password, mockedCarrier);

        // Act
        boolean result = theEmployee.equals(input);

        // Assert
        assertFalse(result);
    }

    @Test
    public void equals_InputIsTheSameEmployee_ReturnsTrue () {
        // Arrange
        long EmployeeId = 50;
        Employee theEmployee = new TestableEmployee(EmployeeId, firstName, surname, birthNumber, password, mockedCarrier);

        // Act
        boolean result = theEmployee.equals(theEmployee);

        // Assert
        assertTrue(result);
    }

    @Test
    public void authenticate_InputIsNull_ReturnsFalse () {
        // Arrange
        String nullBirthNumber = null;
        String nullPassword = null;

        // Act
        boolean result = TestableEmployee.authenticate(nullBirthNumber, nullPassword);

        // Assert
        assertFalse(result);
    }

    @Test
    public void authenticate_BirthNumberIsNullAndPasswordIsCorrect_ReturnsFalse () {
        // Arrange
        String nullBirthNumber = null;

        // Act
        boolean result = TestableEmployee.authenticate(nullBirthNumber, password);

        // Assert
        assertFalse(result);
    }

    @Test
    public void authenticate_BirthNumberIsCorrectAndPasswordIsNull_ReturnsFalse () {
        // Arrange
        String nullPassword = null;

        // Act
        boolean result = TestableEmployee.authenticate(birthNumber, nullPassword);

        // Assert
        assertFalse(result);
    }

    @Test
    public void authenticate_BothInputsAreNotNullButInvalid_ReturnsFalse () {
        // Arrange
        String wrongPassword = "password";
        String wrongBirthNumber = "2222222222";

        // Act
        boolean result = TestableEmployee.authenticate(wrongBirthNumber, wrongPassword);

        // Assert
        assertFalse(result);
    }

    @Test
    public void authenticate_BothInputsAreNotNullPasswordIsCorrectButBirthNumberIsInvalid_ReturnsFalse () {
        // Arrange
        String wrongBirthNumber = "2222222222";

        // Act
        boolean result = TestableEmployee.authenticate(wrongBirthNumber, password);

        // Assert
        assertFalse(result);
    }

    @Test
    public void authenticate_BothInputsAreNotNullPasswordIsInvalidButBirthNumberIsCorrect_ReturnsFalse () {
        // Arrange
        String wrongPassword = "password";

        // Act
        boolean result = TestableEmployee.authenticate(birthNumber, wrongPassword);

        // Assert
        assertFalse(result);
    }
    @Test
    public void authenticate_BothInputsAreCorrect_ReturnsTrue () {
        // Arrange

        // Act
        boolean result = TestableEmployee.authenticate(birthNumber, password);

        // Assert
        assertTrue(result);
    }
}
