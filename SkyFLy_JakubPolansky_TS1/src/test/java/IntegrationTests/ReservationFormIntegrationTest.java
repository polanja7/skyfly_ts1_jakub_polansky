package IntegrationTests;

import dbutils.SkyFlyEMF;
import entities.Airplane;
import entities.Carrier;
import entities.Flight;
import entities.Reservation;
import java.util.Date;
import java.util.Random;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import views.ReservationForm;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManagerFactory;
import javax.swing.JFrame;

public class ReservationFormIntegrationTest {
  private static EntityManager em;
  private static Flight flightSample;
  private static EntityManagerFactory emf;
  
  @BeforeEach
  public void setUp() {
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();
    emf = SkyFlyEMF.getEmf();
    em = emf.createEntityManager();
  }
  
  @BeforeAll
  public static void setUpAll() {
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();
    emf = SkyFlyEMF.getEmf();
    em = emf.createEntityManager();
    em.getTransaction().begin();
    Carrier carrierSample = new Carrier("Lufthansa", "Germany", "1960");
    em.persist(carrierSample);
    em.getTransaction().commit();
        
    em.getTransaction().begin();
    Airplane airplaneSample = new Airplane(60, carrierSample);
    em.persist(airplaneSample);
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    Random random = new Random();
    int price = random.nextInt(300);
    price += 1;
    flightSample = new Flight("PRG", "LHR", 130, new Date(117, 5, 5, 8, 45), price, airplaneSample, null);
    em.persist(flightSample);
    em.flush();
    em.refresh(flightSample);
    em.getTransaction().commit();
  }

  @Test
  public void testCreateValidReservation() {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Reservation r").executeUpdate();
    em.getTransaction().commit();
    
    JFrame frame = new JFrame();
    ReservationForm reservationForm = new ReservationForm(new Reservation("", null, flightSample));
    frame.getContentPane().add(reservationForm);
    reservationForm.getBirthNumberTextField().setText("9602276926");
    reservationForm.getSeats().get(0).setSelected(true);
    reservationForm.getConfirmButton().doClick();
    
    assertEquals(1, em.createQuery("SELECT r FROM Reservation r").getResultList().size());
  }
  
  @Test
  public void testCreateInvalidReservation() {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Reservation r").executeUpdate();
    em.getTransaction().commit();
    
    SkyFlyEMF.reloadEmf();
    JFrame frame = new JFrame();
    ReservationForm reservationForm = new ReservationForm(new Reservation("", null, flightSample));
    frame.getContentPane().add(reservationForm);
    reservationForm.getBirthNumberTextField().setText("");
    reservationForm.getConfirmButton().doClick();
    
    assertEquals(0, em.createQuery("SELECT r FROM Reservation r").getResultList().size());
  }

  @AfterEach
  public void clean() {
    em.close();
    emf.close();
  }
}
