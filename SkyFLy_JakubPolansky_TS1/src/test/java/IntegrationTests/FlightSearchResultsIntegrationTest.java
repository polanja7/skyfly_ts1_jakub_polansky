package IntegrationTests;

import dbutils.SkyFlyEMF;
import entities.Airplane;
import entities.Carrier;
import entities.Flight;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import views.FlightSearchResults;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManagerFactory;
import javax.swing.table.DefaultTableModel;


public class FlightSearchResultsIntegrationTest {
  private static EntityManager em;
  private static Airplane airplaneSample;
  private static EntityManagerFactory emf;
  
  @BeforeAll
  public static void setUpClass() {
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();

    emf = SkyFlyEMF.getEmf();
    em = emf.createEntityManager();
    
    em.getTransaction().begin();
    Carrier carrierSample = new Carrier("Lufthansa", "Germany", "1960");
    em.persist(carrierSample);
    em.getTransaction().commit();
        
    em.getTransaction().begin();
    airplaneSample = new Airplane(60, carrierSample);
    em.persist(airplaneSample);
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    Flight flightSample = new Flight("PRG", "LHR", 130, new Date(117, 5, 5, 8, 45), 100, airplaneSample, null);
    em.persist(flightSample);
    em.flush();
    em.refresh(flightSample);
    em.getTransaction().commit();
  }

  @Test
  public void testFillTableWithFlights() {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Reservation r").executeUpdate();
    em.createQuery("DELETE FROM Flight f").executeUpdate();
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    Flight flight1 = new Flight("PRG", "LHR", 130, new Date(117, 5, 5, 8, 45), 100, airplaneSample, null);
    em.persist(flight1);
    em.flush();
    em.refresh(flight1);
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    Flight flight2 = new Flight("AMS", "MAD", 130, new Date(117, 5, 5, 10, 45), 100, airplaneSample, null);
    em.persist(flight2);
    em.flush();
    em.refresh(flight2);
    em.getTransaction().commit();
    
    List<Flight> flights = new ArrayList<>();
    flights.add(flight1);
    flights.add(flight2);
    
    FlightSearchResults flightSearchResults = new FlightSearchResults(flights);
    
    List<String> flightIds = new ArrayList<>();
    
    for (int row = 0; row < flightSearchResults.getFlightResultsTable().getRowCount(); row++){
      DefaultTableModel model = (DefaultTableModel)flightSearchResults.getFlightResultsTable().getModel();
      flightIds.add(model.getValueAt(row, 0).toString());
    }
    
    assertEquals(2, flightIds.size());
    assertFalse(flightIds.contains(""));
    assertTrue(flightIds.contains(flight1.getId().toString()));
    assertTrue(flightIds.contains(flight2.getId().toString()));
  }
  @AfterAll
  public static void clean() {
    em.close();
    emf.close();
  }
}
