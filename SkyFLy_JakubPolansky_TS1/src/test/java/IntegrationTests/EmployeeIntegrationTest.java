package IntegrationTests;

import dbutils.SkyFlyEMF;

import javax.persistence.EntityManager;

import entities.Carrier;
import entities.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeIntegrationTest {
  private static EntityManager em;
  
  @BeforeEach
  public void setup() {
    Employee.setCurrentEmployee(null);
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();
    em = SkyFlyEMF.getEmf().createEntityManager();
  }

  @Test
  public void authenticate_ValidData_ReturnsTrue() {
    em.getTransaction().begin();
    Carrier carrierSample = new Carrier("Lufthansa", "Germany", "1960");
    em.persist(carrierSample);
    em.getTransaction().commit();
    em.getTransaction().begin();
    Random random = new Random();
    int number;
    String fullNumber = "";
    for (int i = 0; i < 10; i++) {
      number = random.nextInt(9);
      fullNumber += number;
    }
    Employee employeeSample = new Employee("James", "Doe", fullNumber, "password", carrierSample);
    em.persist(employeeSample);
    em.getTransaction().commit();
    boolean result = Employee.authenticate(fullNumber, "password");
    assertTrue(result);
  }
  
  @Test
  public void authenticate_InvalidData_ReturnsFalse() {
    boolean result = Employee.authenticate("invalid", "invalid");
    assertFalse(result);
  }

  @AfterEach
  public void close() {
    em.close();
  }
}
