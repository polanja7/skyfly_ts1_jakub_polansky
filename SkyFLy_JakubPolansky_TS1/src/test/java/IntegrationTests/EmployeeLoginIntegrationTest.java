package IntegrationTests;

import dbutils.SkyFlyEMF;
import entities.Carrier;
import entities.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import views.EmployeeDashboard;
import views.EmployeeLogin;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Component;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.swing.JFrame;


public class EmployeeLoginIntegrationTest {
  private static EntityManager em;
  
  @BeforeEach
  public void setUp() {
    Employee.setCurrentEmployee(null);
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();
    em = SkyFlyEMF.getEmf().createEntityManager();
  }

  @Test
  public void testValidLogin() {
    em.getTransaction().begin();
    Carrier carrierSample = new Carrier("Lufthansa", "Germany", "1960");
    em.persist(carrierSample);
    em.getTransaction().commit();

    em.getTransaction().begin();
    Random random = new Random();
    int number;
    String fullNumber = "";
    for (int i = 0; i < 10; i++) {
      number = random.nextInt(9);
      fullNumber += number;
    }
    Employee employeeSample = new Employee("John", "Doe", fullNumber, "password", carrierSample);
    em.persist(employeeSample);
    em.getTransaction().commit();

    JFrame frame = new JFrame();
    EmployeeLogin employeeLogin = new EmployeeLogin();
    frame.getContentPane().add(employeeLogin);
    employeeLogin.getBirthNumberTextField().setText(employeeSample.getBirthNumber());
    employeeLogin.getPasswordTextField().setText(employeeSample.getPassword());
    
    employeeLogin.getConfirmButton().doClick();
    
    int renderedDashboardPanels = 0;
    
    for (Component c : frame.getContentPane().getComponents()) {
      if(c instanceof EmployeeDashboard) {
        renderedDashboardPanels += 1;
      }
    }
    assertEquals(1, renderedDashboardPanels);
    assertEquals(employeeSample, Employee.getCurrentEmployee());
  }
  
  @Test
  public void testInvalidLogin() {
    JFrame frame = new JFrame();
    EmployeeLogin employeeLogin = new EmployeeLogin();
    frame.getContentPane().add(employeeLogin);
    
    employeeLogin.getBirthNumberTextField().setText("wrong");
    employeeLogin.getPasswordTextField().setText("wrong");
    
    employeeLogin.getConfirmButton().doClick();
    
    int renderedDashboardPanels = 0;
    int renderedLoginPanels = 0;
    
    for (Component c : frame.getContentPane().getComponents()) {
      if(c instanceof EmployeeDashboard) {
        renderedDashboardPanels += 1;
      }
      
      if(c instanceof EmployeeLogin) {
        renderedLoginPanels += 1;
      }
    }
    
    assertEquals( 0, renderedDashboardPanels);
    assertEquals(1, renderedLoginPanels);
    assertNull(Employee.getCurrentEmployee());
  }
  @AfterEach
  public void close() {
    em.close();
  }
}
