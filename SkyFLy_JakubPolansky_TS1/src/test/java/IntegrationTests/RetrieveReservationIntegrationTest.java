package IntegrationTests;

import dbutils.SkyFlyEMF;
import entities.Airplane;
import entities.Carrier;
import entities.Flight;
import entities.Reservation;
import java.awt.Component;
import java.util.Date;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.swing.JFrame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import views.RetrieveReservation;
import views.RetrieveReservationResults;

import static org.junit.jupiter.api.Assertions.*;

public class RetrieveReservationIntegrationTest {
  private static EntityManager em;
  private static EntityManagerFactory emf;
  private static Flight flightSample;
  private static Reservation reservationSample;

  @BeforeEach
  public void eachSetUp(){
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();
    emf = SkyFlyEMF.getEmf();
    em = emf.createEntityManager();
  }
  
  @BeforeAll
  public static void setUp() {
    SkyFlyEMF.setUnitName("CVUT_DBS_PU");
    SkyFlyEMF.reloadEmf();
    emf = SkyFlyEMF.getEmf();
    em = emf.createEntityManager();
    em.getTransaction().begin();
    Carrier carrierSample = new Carrier("Lufthansa", "Germany", "1960");
    em.persist(carrierSample);
    em.getTransaction().commit();
        
    em.getTransaction().begin();
    Airplane airplaneSample = new Airplane(60, carrierSample);
    em.persist(airplaneSample);
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    Random random = new Random();
    int price = random.nextInt(300);
    price += 1;
    flightSample = new Flight("PRG", "AMS", 130, new Date(117, 5, 5, 8, 45), price, airplaneSample, null);
    em.persist(flightSample);
    em.getTransaction().commit();
  }

  @Test
  public void testValidReservationRetrieval() {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Reservation r").executeUpdate();
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    reservationSample = new Reservation("9602276926", 1, flightSample);
    em.persist(reservationSample);
    em.getTransaction().commit();
    
    JFrame frame = new JFrame();
    RetrieveReservation retrieveReservation = new RetrieveReservation();
    frame.getContentPane().add(retrieveReservation);
    
    retrieveReservation.getBirthNumberTextField().setText(reservationSample.getPassengerBirthNumber());
    
    retrieveReservation.getConfirmButton().doClick();
    
    RetrieveReservationResults retrieveReservationResults = null;
    
    for (Component c : frame.getContentPane().getComponents()) {
      if(c instanceof RetrieveReservationResults) {
        retrieveReservationResults = (RetrieveReservationResults) c;
        break;
      }
    }
    
    assertEquals(1, retrieveReservationResults.getReservationsTable().getRowCount());
  }
  
  @Test
  public void testInvalidReservationRetrieval() {
    em.getTransaction().begin();
    em.createQuery("DELETE FROM Reservation r").executeUpdate();
    em.getTransaction().commit();
    
    em.getTransaction().begin();
    reservationSample = new Reservation("9602276926", 1, flightSample);
    em.persist(reservationSample);
    em.getTransaction().commit();
    
    JFrame frame = new JFrame();
    RetrieveReservation retrieveReservation = new RetrieveReservation();
    frame.getContentPane().add(retrieveReservation);
    
    retrieveReservation.getBirthNumberTextField().setText("1111111111");
    
    retrieveReservation.getConfirmButton().doClick();
    
    RetrieveReservationResults retrieveReservationResults = null;
    
    for (Component c : frame.getContentPane().getComponents()) {
      if(c instanceof RetrieveReservationResults) {
        retrieveReservationResults = (RetrieveReservationResults) c;
        break;
      }
    }
    
    assertEquals(0,retrieveReservationResults.getReservationsTable().getRowCount());
  }
  @AfterEach
  public void clean(){
    em.close();
    emf.close();
  }
}
