package RefactoredClassesForTesting;

import entities.Carrier;

public class TestableCarrier extends Carrier {
    public Long id;

    public TestableCarrier(Long id, String name, String country, String yearOfFoundation) {
        super(name, country, yearOfFoundation);
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carrier)) {
            return false;
        }
        Carrier other = (TestableCarrier) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public Long getId () {
        return id;
    }
}
