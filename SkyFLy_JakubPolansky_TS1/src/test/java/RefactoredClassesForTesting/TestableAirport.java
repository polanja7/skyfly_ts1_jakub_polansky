package RefactoredClassesForTesting;

import entities.Airport;

public class TestableAirport extends Airport {
    public Long id;

    public TestableAirport(Long id, String airportName, String airportShortcut) {
        super(airportName, airportShortcut);
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Airport)) {
            return false;
        }
        Airport other = (TestableAirport) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public Long getId () {
        return id;
    }
}
