package RefactoredClassesForTesting;

import entities.Airplane;
import entities.Flight;
import entities.Reservation;

import java.util.Date;
import java.util.List;

public class TestableFlight extends Flight {
    public Long id;

    public TestableFlight(Long id, String departureAirport, String arrivalAirport, int duration, Date timeOfDeparture, int price, Airplane airplane, List<Reservation> reservations) {
        super(departureAirport, arrivalAirport,duration, timeOfDeparture, price, airplane, reservations);
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flight)) {
            return false;
        }
        Flight other = (TestableFlight) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public Long getId () {
        return id;
    }
}
