package RefactoredClassesForTesting;

import entities.Airplane;
import entities.Carrier;

public class TestableAirplane extends Airplane {
    public Long id;

    public TestableAirplane(Long id, int capacity, Carrier carrier) {
        super(capacity, carrier);
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Airplane)) {
            return false;
        }
        Airplane other = (TestableAirplane) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }
    @Override
    public Long getId () {
        return id;
    }
}
