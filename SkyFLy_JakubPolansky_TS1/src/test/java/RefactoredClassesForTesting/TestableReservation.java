package RefactoredClassesForTesting;

import entities.Airplane;
import entities.Flight;
import entities.Reservation;

import java.util.Date;
import java.util.List;

public class TestableReservation extends Reservation {
    public Long id;

    public TestableReservation(Long id, String passengerBirthNumber, Integer seatNumber, Flight flight) {
        super(passengerBirthNumber, seatNumber, flight);
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (TestableReservation) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public Long getId () {
        return id;
    }
}
