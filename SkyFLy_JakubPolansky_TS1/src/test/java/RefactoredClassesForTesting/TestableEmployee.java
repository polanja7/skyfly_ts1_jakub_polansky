package RefactoredClassesForTesting;

import dbutils.SkyFlyEMF;
import entities.Carrier;
import entities.Employee;
import entities.Flight;
import entities.Reservation;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestableEmployee extends Employee {
    public Long id;
    public static Employee currentEmployee;

    public TestableEmployee(Long id, String firstName, String surname, String birthNumber, String password, Carrier carrier) {
        super(firstName, surname, birthNumber, password, carrier);
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (TestableEmployee) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public Long getId () {
        return id;
    }

    public static Employee findByBirthNumber(String birthNumber) {
        Employee mockedEmployee = mock(Employee.class);
        when(mockedEmployee.getPassword()).thenReturn("heslo");
        if (birthNumber == "1111111111") {
            return mockedEmployee;
        }
        return null;
    }

    public static boolean authenticate (String birthNumber, String password) {
        Employee employee = findByBirthNumber(birthNumber);

        if (employee != null) {
            if (employee.getPassword().equals(password)) {
                currentEmployee = employee;
                return true;
            }
        }
        return false;
    }
}
